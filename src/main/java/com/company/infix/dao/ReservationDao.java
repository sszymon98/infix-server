package com.company.infix.dao;

import com.company.infix.dto.CarDto;
import com.company.infix.dto.ReservationDto;
import com.company.infix.service.CheckValues;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ReservationDao {
    @Autowired
    JdbcTemplate jdbc;
    @Autowired
    CheckValues chkVal;

    public ResponseEntity<String> testReservation(ReservationDto resDb) {
        String desc = resDb.getDescription();
        String id_car = jdbc.queryForObject("SELECT idcar from car where vin=?", new Object[]{resDb.getVin()}, String.class);
        if (chkVal.checkDesc(desc)) {
            jdbc.update("INSERT INTO reservation(iduser,idcar,date_start,date_finish,status,description) VALUES (?,?,?,?,?,?)",
                    resDb.getIdUser(), id_car, resDb.getDateStart(), null, "1", desc);
            return new ResponseEntity<>("1", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
    public String testShowRes(String login){
        ArrayList arr;
        arr = jdbc.query("select u.name,u.surname,u.tele_no,u.email,re.date_start,c.model,c.marka,re.status,c.vin,re.description,re.idreservation,r.status as test\n" +
                        "from reservation re inner join car c on re.idcar = c.idcar inner join user u on c.iduser = u.iduser inner join repair r on c.idcar = r.idcar  where login=?",new Object[]{login},
                rs->{
                    ArrayList<HashMap<String,Object>> lis = new ArrayList<>();
                    while(rs.next()){
                        HashMap<String,Object> entry = new HashMap<>();
                        for(int i=1;i<=rs.getMetaData().getColumnCount();i++){
                            entry.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
                        }
                        lis.add(entry);
                    }
                    return lis;
                });
        return new Gson().toJson(arr);
    }
}
