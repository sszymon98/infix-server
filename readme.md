# Endpointy


## Rejestrowanie użytkownika
POST http://localhost:8080/register 
Wysyła JSON’a:
```json
{
    "name":"Jan",
    "surname":"Kowalski",
    "pesel":"12312312312",
    "driversLicense":"54321/11/4344",
    "password":"123",
    "login":"pracownik1",
    "email":"szymon.zsp5@gmail.com",
    "telephoneNumber":"123456789"
}
```
```java
Zwraca kod:
200 OK - jeśli utworzyło
409 Conflict - jeśli napotkało jakiś błąd 
```

## Logowanie użytkownika
POST http://localhost:8080/login
Wysyła JSON’a:
```json
{
    "login":"adminek",
    "password":"321123"
}
```
```java
Zwraca kod:
200 OK - zalogowano
409 Conflict - błąd logowania
```

## Dodawanie samochodu
POST http://localhost:8080/add-car/{login}
Wysyła JSON’a:
```json
{
    "marka":"bmw",
    "model":"m5",
    "engineCapacity":"3",
    "vin":"12345678909876543",
    "yearOf":"2019",
    "idUser":"17"
}
```
```java
Zwraca kod:
200 OK - dodano
409 Conflict - błąd dodawania
```

## Otrzymanie danych rezerwacji danego użytkownika
GET http://localhost:8080/add-res/{login}
Zwraca JSON’a:
```json
{
    "idCar":"4",
    "marka":"Opel",    
    "vin":"12345678909876543",    
    "idUser":"17"
}
```

## Dodawanie rezerwacji
POST http://localhost:8080/add
Wysyła JSON’a:
```json
{
    "idUser":"18",
    "dateStart":"2002-03-21",    
    "vin":"12345678909876543",    
    "description":"Wymiana oleju"
}
```
```java
Zwraca kod:
200 OK - dodano
409 Conflict - błąd dodawania
```


## Dodawanie nowej naprawy
POST http://localhost:8080/add-repair/{login} 
Wysyła JSON’a:
```json
{    
    "vin":"1234576891234586"
}
```
```java
Zwraca kod:
200 OK - dodano
409 Conflict - błąd dodawania
```

## Dane konkretnego użytkownika
GET http://localhost:8080/edit/{login}
Zwraca JSON’a:
```json
[
    {
        "email": "szymon.zsp5@gmail.com",
        "telephoneNumber": "123456789"
    }
]
```

## Edycja danych użytkownika
PUT http://localhost:8080/edit-send
Wysyła JSON’a:
```json
{
    "login":"szymek",
    "email":"123@onet.eu",
    "telephoneNumber":"876352132",
    "password":"321"
}
```
```java
Zwraca kod:
200 OK - edytowano
409 Conflict - błąd edycji
```

## Edycja permisji użytkownika
PUT http://localhost:8080/edit-send
Wysyła JSON’a:
```json
{
    "login":"szymek",
    "permision":"1"   
}
```
```java
Zwraca kod:
200 OK - edytowano
409 Conflict - błąd edycji
```
## Historia rezerwacji użytkownika
GET http://localhost:8080/show-history/{login}
Zwraca JSON’a:
```json
{
    "marka":"bmw",
    "model":"m5",
    "dateStart":"2020-03-21",
    "vin":"12345678909876543",
    "dateFinish":"2020-04-22"   
}
```

## Dane naprawy o konkretnym vinie
GET http://localhost:8080/search/{vin}
Zwraca JSON’a:
```json
{
    "status":"Naprawa silnika",
    "name":"Maciej",
    "surname":"Biel" 
}

```
## Informacje o pojeździe
GET http://localhost:8080/get-car/{login}
Zwraca JSON’a:
```json
{
    "marka":"bmw",
    "model":"m5",
    "engineCapacity":"3600",
    "vin":"12345678909876543",
    "yearOf":"2019"   
}
```
## Dodawanie nowej wyceny
POST http://localhost:8080/add-pricing
Wysyła JSON’a:
```json
{
    "idUser":"17",
    "description":"Wymiana oleju"    
}
```

## Edycja ceny wyceny o danym id
PUT http://localhost:8080/update-pricing{id}
Wysyła JSON’a:
```json
{
    "price":"300"    
}
```
```java
Zwraca kod:
200 OK - dodano
409 Conflict - błąd dodawania
```

## Informacje o wszystkich wycenach
GET http://localhost:8080/show-pricing/
Zwraca JSON’a:
```json
{
    "idPricing":"1",
    "description":"Wymiana oleju",
    "price":"300",    
    "idUser":"17"   
}
```
## Informacje o wycenach użytkownika
GET http://localhost:8080/show-pricing/{user}
Zwraca JSON’a:
```json
{
    "idPricing":"1",
    "description":"Wymiana oleju",
    "price":"300",    
    "idUser":"17"   
}
```

## Informacje o wszystkich użytkownikach
GET http://localhost:8080/show-allusers/
Zwraca JSON’a:
```json
{
    "permision": "0",
    "name": "Maciej",
    "surname": "Biel",
    "pesel": "88051710625",
    "driversLicense": "55555/22/1234",
    "login": "maciej123",
    "email": "maciek@gmail.com",
    "telephoneNumber": "876544321",
    "idUser": "1"
 }
```

## Informacje o wszystkich samochodach
GET http://localhost:8080/show-allcars/
Zwraca JSON’a:
```json
[
    {
        "surname": "Biel",
        "name": "Maciej",
        "marka": "Opel",
        "model": "Astra",
        "vin": "WP0ZZZ99ZTS392124",
        "tele_no": "876544321",
        "engine_capacity": "1395",
        "email": "maciek@gmail.com",
        "year_of": "2006"
    }
]
```
## Informacje o wszystkich naprawach
GET http://localhost:8080/show-allrepair/
Zwraca JSON’a:
```json
[
    {
        "surname": "Kowalski",
        "name": "Jan",
        "date_finish": "cze 28, 2020",
        "marka": "Citroen",
        "vin": "12345678900987654",
        "model": "Berligo",
        "tele_no": "123456789",
        "email": "unet123@onet.eu",
        "status": "ukonczono"
    }
]
```
## Informacje o naprawach danego użytkownika
GET http://localhost:8080/show-allrepair/{login}
Zwraca JSON’a:
```json
[
    {
        "surname": "Kowalski",
        "name": "Jan",
        "date_finish": "cze 28, 2020",
        "marka": "Citroen",
        "vin": "12345678900987654",
        "model": "Berligo",
        "tele_no": "123456789",
        "email": "unet123@onet.eu",
        "status": "ukonczono"
    }
]
```

## Edycja danych użytkownika
PUT http://localhost:8080/change-status/{flag}
Wstawienie flagi “1” konczy naprawę
Wysyła JSON’a:
```json
{
    "status":"Naprawiono koło",    
}
```
```java
Zwraca kod:
200 OK - edytowano status
409 Conflict - błąd edycji
```
## Wszyscy pracownicy
GET http://localhost:8080/show-workers
Zwraca JSON’a:
```json
[
    {
        "name": "Marek",
        "surname": "Oko"
    }
]
```
## Zmiana statusu rezerwacji
PUT http://localhost:8080/edit-reservation{idres}?status1=3
```java
Zwraca kod:
200 OK - edytowano status
409 Conflict - błąd edycji
```

## Wszystkie rezerwacje
GET http://localhost:8080/show-pending
Zwraca JSON’a:
```json
[
    {
        "idReservation": "1",
        "idUser": "1",
        "idCar": "1",
        "dateStart": "2020-06-29",
        "dateFinish": "2020-06-27",
        "status": "2",
        "description": "Potrzeba wymienic olej"
    }
]
```