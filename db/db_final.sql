-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: infix
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `car` (
  `idcar` int NOT NULL AUTO_INCREMENT,
  `marka` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `engine_capacity` varchar(45) DEFAULT NULL,
  `vin` varchar(20) DEFAULT NULL,
  `year_of` varchar(4) DEFAULT NULL,
  `iduser` int DEFAULT NULL,
  PRIMARY KEY (`idcar`),
  KEY `iduser_idx` (`iduser`),
  CONSTRAINT `id_user` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (16,'Citroen','Berligo','2000','12345678900987654','2001',18),(17,'Fiat','Bravo','1900','92312348909833121A','2009',17),(18,'tojota','raf','1000','00000000AA0000000','2010',19),(19,'ford','gownowort','2300','12345678901234567','2001',21);
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pricing`
--

DROP TABLE IF EXISTS `pricing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pricing` (
  `idpricing` int NOT NULL AUTO_INCREMENT,
  `description` mediumtext,
  `price` varchar(45) DEFAULT NULL,
  `iduser` int NOT NULL,
  `idcar` int DEFAULT NULL,
  PRIMARY KEY (`idpricing`),
  KEY `iduser_idx` (`iduser`),
  KEY `idcar1_idx` (`idcar`),
  CONSTRAINT `idcar2` FOREIGN KEY (`idcar`) REFERENCES `car` (`idcar`),
  CONSTRAINT `iduser1` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pricing`
--

LOCK TABLES `pricing` WRITE;
/*!40000 ALTER TABLE `pricing` DISABLE KEYS */;
INSERT INTO `pricing` VALUES (1,'Ile to bedzie kosztować, zepsuty mam XDDD','1000',18,NULL);
/*!40000 ALTER TABLE `pricing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repair`
--

DROP TABLE IF EXISTS `repair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `repair` (
  `idrepair` int NOT NULL AUTO_INCREMENT,
  `iduser` int DEFAULT NULL,
  `status` mediumtext,
  `vin` varchar(20) DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `idcar` int DEFAULT NULL,
  PRIMARY KEY (`idrepair`),
  KEY `idcar2_idx` (`idcar`),
  CONSTRAINT `idcar5` FOREIGN KEY (`idcar`) REFERENCES `car` (`idcar`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repair`
--

LOCK TABLES `repair` WRITE;
/*!40000 ALTER TABLE `repair` DISABLE KEYS */;
INSERT INTO `repair` VALUES (7,18,'ukonczono','92312348909833121A','2020-06-29',17),(8,19,'ukonczono','00000000AA0000000','2020-06-29',18);
/*!40000 ALTER TABLE `repair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservation` (
  `idreservation` int NOT NULL AUTO_INCREMENT,
  `iduser` int NOT NULL,
  `idcar` int NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `description` mediumtext,
  PRIMARY KEY (`idreservation`),
  KEY `iduser_idx` (`iduser`),
  KEY `idcar_idx` (`idcar`),
  CONSTRAINT `idcar` FOREIGN KEY (`idcar`) REFERENCES `car` (`idcar`),
  CONSTRAINT `iduser` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (4,18,16,NULL,'2020-06-28','3','Wymiana oleju'),(5,18,16,NULL,NULL,'1','Wymiana oleju'),(6,19,18,'2020-12-12','2020-06-29','2','korbe ukreciło'),(7,21,19,'2020-06-12',NULL,'1','opis');
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `iduser` int NOT NULL AUTO_INCREMENT,
  `permision` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `pesel` varchar(45) DEFAULT NULL,
  `drivers_license` varchar(45) DEFAULT NULL,
  `password` longtext,
  `login` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `tele_no` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (17,'0','Szymon','Bilinski','12345678901','12345/23/4444','d8578edf8458ce06fbc5bb76a58c5ca4','root','123@onet.eu','999999999'),(18,'1','Jan','Kowalski','12312312312','54321/11/4344','d8578edf8458ce06fbc5bb76a58c5ca4','pracownik1','onet1001@onet.eu','123456789'),(19,'1','Kornel','Kornel','99999999999','12345/22/4444','202cb962ac59075b964b07152d234b70','kornel1','a123@gmail.com','123456789'),(20,'2','Admin','Admin','12345678901','12345/22/3333','202cb962ac59075b964b07152d234b70','admin','szymon.zsp5@gmail.com','123456789'),(21,'0','Andrzej','Andrzej','12345678901','12345/12/1234','202cb962ac59075b964b07152d234b70','anio','szymon.zsp5@gmail.com','123456789');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-29  4:09:29
